let x = 6;
let y = 14;
let z = 4;
let res = x += y - x++ * z; //  x += y <=>	x = x + y // x++ = x + 1 // Приоритетность : x++ >>> * z >>> y - >>> x+= .
document.write(`${res} <br>`); 
x = 6;
y = 14;
z = 4;
res = z = --x - y * 5;      //  --x = x - 1 // Приоритетность : --x >>> y * 5 >>> x - y >>> = z .
document.write(`${res} <br>`);
x = 6;
y = 14;
z = 4;
res = y /= x + 5 % z;       //  y /= x <=> y = y/x // 5 % z <=> остаток от деления // Приоритетность : 5 % z >>> + x >>> y /= .
document.write(`${res} <br>`);
x = 6;
y = 14;
z = 4;
res = z - x++ + y * 5;      //  Приоритетность : x++ >>> y * 5 >>> z - x >>> + .
document.write(`${res} <br>`);
x = 6;
y = 14;
z = 4;
res = x = y - x++ * z;      //  Приоритетность : x++ >>> * z >>> y - >>> x = .
document.write(`${res} <br>`);